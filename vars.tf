variable "project_name" {}
variable "docker_cache_enabled" {
  type = bool
  default = true
}
variable "ecr_name" {
  default = null
}
variable "s3_buckets" {
  default = null
  type = list(string)
}
variable "dynamodb_tables" {
  default = null
  type = list(string)
}
variable "buildspec_file" {
  type = string
  default = "buildspec.yml"
}
variable "environment_variables" {
  default = {}
  type    = map(string)
}
variable "sns_topics" {
  default = null
  type = list(string)
}
variable "custom_policy" {
  default = null
  type = string
}
variable "ssm_parameters" {
  default = {}
  type    = map(string)
}