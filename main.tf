resource "aws_s3_bucket" "codebuild" {
  bucket = var.project_name
  acl = "private"

  lifecycle_rule {
    noncurrent_version_expiration {
      days = 30
    }

    enabled = true
  }

  versioning {
    enabled = true
  }
  
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_codebuild_project" "codebuild" {
  name = var.project_name
  service_role  = aws_iam_role.codebuild.arn

  artifacts {
    type = "NO_ARTIFACTS"
  }

  cache {
    type = var.docker_cache_enabled == true ? "LOCAL" : null
    modes = [var.docker_cache_enabled == true ? "LOCAL_DOCKER_LAYER_CACHE" : ""]
  }

  environment {
    compute_type = "BUILD_GENERAL1_MEDIUM"
    image = "aws/codebuild/standard:4.0"
    type = "LINUX_CONTAINER"
    privileged_mode = true

    environment_variable {
      name  = "DOCKER_CLIENT_TIMEOUT"
      value = "120"
    }

    environment_variable {
      name  = "COMPOSE_HTTP_TIMEOUT"
      value = "120"
    }

    environment_variable {
      name  = "DOCKERHUB_USERNAME"
      value = "/dockerhub/username"
      type = "PARAMETER_STORE"
    }

    environment_variable {
      name  = "DOCKERHUB_PASSWORD"
      value = "/dockerhub/password"
      type = "PARAMETER_STORE"
    }

    dynamic "environment_variable" {
      for_each = var.environment_variables

      content {
        name  = environment_variable.key
        value = environment_variable.value
      }
    }

    dynamic "environment_variable" {
      for_each = var.ssm_parameters

      content {
        name  = environment_variable.key
        value = environment_variable.value
        type = "PARAMETER_STORE"
      }
    }
  }

  source {
    type = "S3"
    location = "${aws_s3_bucket.codebuild.bucket}/${var.project_name}.zip"
    buildspec = var.buildspec_file
  }
}
