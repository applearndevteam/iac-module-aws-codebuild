
# iac-module-aws-codebuild

Terraform module which creates [AWS CodeBuild infrastructure](https://docs.aws.amazon.com/codebuild/latest/userguide/welcome.html) on AWS. The created CodeBuild project will by default have access to Docker Hub credentials. They can be accessed with the `$DOCKERHUB_USERNAME` and `$DOCKERHUB_PASSWORD` environment variables. Example of Docker login usage:

```sh
echo $DOCKERHUB_PASSWORD | docker login --username $DOCKERHUB_USERNAME --password-stdin
```

**Note: requires `terraform >= 0.12.0`**
## Usage

```
module "codebuild" {
  source = "bitbucket.org/applearndevteam/iac-module-aws-codebuild.git"
  project_name = "name-of-project"
  s3_buckets = ["my-bucket"]
}
```


## Inputs

|Name|Description|Type|Default|Required|
|---|---|---|---|---|
|project_name|Name of CodeBuild project and S3 bucket to create. E.g `"my-web-app-pr-builder"`|string|n/a|yes|
|docker_cache_enabled|Should CodeBuild cache Docker layers during the build process? This speeds up builds|bool|true|no|
|ecr_name|Name of Elastic Container Registry to give CodeBuild access to if required|string|`null`|no|
|s3_buckets|List of S3 buckets CodeBuild should have access to if required|list|`null`|no|
|dynamodb_tables|List of DynamoDB tables CodeBuild should have access to if required|list|`null`|no|
|sns_topics|List of SNS topics CodeBuild should have access to if required|list|`null`|no|
|buildspec_file|Name of the buildspec file to use|string|"buildspec.yml"|no|
|environment_variables|Map of environment variables to pass to CodeBuild, can be referenced in buildspec.yml|map(string)|{}|no|
|custom_policy|Custom IAM policy to pass to CodeBuild if required, pass as JSON string|string|`null`|no|
|ssm_parameters|Map of SSM parameters to pass to CodeBuild, can be referenced in buildspec.yml|map(string)|{}|no|